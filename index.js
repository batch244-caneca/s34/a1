const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	},
	{
		username: "skypodcast",
		email: "skypodcast@mail.com",
		password: "skyfam"
	}
];


app.get("/home", (req, res) => {
	res.send("Welcome to the homepage")
})


app.get("/users", (req, res) => {
	res.send(users);
})

app.delete("/delete-user", (req, res) => {
	let popped = users.pop(); 
	res.send("User has been deleted");
})




app.listen(port, () => console.log(`Server is running at port ${port}`))